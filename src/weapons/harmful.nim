import
  godot,
  godotapi / [area_2d]

gdobj Harmful of Area2D:
  var
    damage*{.gdExport.}: float32
    time*{.gdExport.}: float32
    power*{.gdExport.}: Vector2
