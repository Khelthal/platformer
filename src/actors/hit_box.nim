import
  godot,
  godotapi / [area_2d, timer],
  weapons / [harmful]

export
  area_2d,
  harmful,
  timer

gdobj HitBox of Area2D:
  var
    collisions*: seq[Harmful]
    timer*: Timer
    timing*{.gdExport.}: float32

  method init*() =
    self.addUserSignal("invulnerable")
    self.addUserSignal("vulnerable")

  method ready*() =
    self.timer = self.getNode("Timer") as Timer
    discard self.connect("area_entered", self, "on_hit_box_area_entered")
    discard self.connect("area_exited", self, "on_hit_box_area_exited")
    discard self.timer.connect("timeout", self, "on_hit_box_timer_timeout")

  proc onHitBoxAreaEntered*(area: Area2D){.gdExport.} =
    if area of Harmful:
      self.collisions.add(area as Harmful)

  proc onHitBoxAreaExited*(area: Area2D){.gdExport.} =
    if area of Harmful:
      self.collisions.del(self.collisions.find(area as Harmful))

  proc onHitBoxTimerTimeout*(){.gdExport.} =
    self.monitoring = true
    self.emitSignal("vulnerable")

  proc deactivate*() =
    self.monitoring = false
    self.timer.waitTime = self.timing
    self.timer.start
    self.emitSignal("invulnerable")

  proc isColliding*(): bool =
    return self.collisions.len > 0

  proc maxDamageCollider*(): Harmful =
    if not self.isColliding:
      return nil
    var
      maxDamageCollider: Harmful = self.collisions[0]
      collider: Harmful
    
    for i in 0..<self.collisions.len:
      collider = self.collisions[i]
      if collider.damage > maxDamageCollider.damage:
        maxDamageCollider = collider
    return maxDamageCollider
