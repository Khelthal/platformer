import
  godot,
  godotapi / [node, texture_progress, tween],
  util / [math]

gdobj PlayerBars of Node:
  var
    healthbar: TextureProgress
    manabar: TextureProgress
    tween: Tween

  method ready*() =
    self.healthbar = self.getNode("Healthbar") as TextureProgress
    self.manabar = self.getNode("Manabar") as TextureProgress
    self.tween = self.getNode("Tween") as Tween

  proc updateHealth*(health: int, maxHealth: int) =
    discard self.tween.interpolateProperty(self.healthbar, "value", self.healthbar.value.toVariant, percentage(maxHealth, health).int.toVariant, 0.5, TRANS_LINEAR, EASE_IN_OUT)
    discard self.tween.start()

  proc updateEnergy*(energy: int, maxEnergy: int) =
    discard self.tween.interpolateProperty(self.manabar, "value", self.manabar.value.toVariant, percentage(maxEnergy, energy).int.toVariant, 0.25, TRANS_LINEAR, EASE_IN_OUT)
    discard self.tween.start()
