import
  godot,
  godotapi / [input],
  input / [i_input_provider],
  actors/player / [player_input]

export
  player_input

gdobj PlayerInputProvider of IInputProvider:
  
  method getInput*(controller: Node): IInput =
    var input: PlayerInput = gdnew[PlayerInput]()
    input.movDir = int(getActionStrength("mov_right") - getActionStrength("mov_left"))
    input.jump = isActionJustPressed("jump")
    input.stopJump = isActionJustReleased("jump")
    input.dash = isActionJustPressed("dash")
    input.stopDash = isActionJustReleased("dash")
    input.attack = isActionJustPressed("attack")
    return input
