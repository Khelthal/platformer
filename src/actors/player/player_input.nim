import
  godot,
  input / [i_input]

export
  i_input

gdobj PlayerInput of IInput:
  var
    jump*: bool
    stopJump*: bool
    attack*: bool
    movDir*: int
    dash*: bool
    stopDash*: bool
