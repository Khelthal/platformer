import
  godot,
  godotapi / [node],
  input / [i_input],
  states / [i_state],
  actors/player / [player_controller, player_input]

export
  i_state

gdobj PlayerSwordAttack of IState:
  var
    finish: bool

  method update*(delta: float64, input: IInput, controlled: Node): string =
    var controller: PlayerController = controlled as PlayerController
    if controller.hitbox.isColliding:
      return "Hurt"
    if not controller.isOnFloor:
      return "Jump"
    if self.finish:
      return "Stand"

  method enter*(controlled: Node) =
    self.finish = false
    var controller: PlayerController = controlled as PlayerController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    controller.velocity = Vector2(x: 0.0, y: 0.0)
    stateMachine.travel("SwordAttack1")

  method exit*(controlled: Node) =
    discard

  proc onAttackFinished*(){.gdExport.} =
    self.finish = true
