import
  godot,
  godotapi / [node],
  main,
  input / [i_input],
  states / [i_state],
  actors/player / [player_controller, player_input],
  util / [math]

export
  i_state

gdobj PlayerHurt of IState:
  var
    finish: bool

  method update*(delta: float64, input: IInput, controlled: Node): string =
    var controller: PlayerController = controlled as PlayerController
    let input: PlayerInput = input as PlayerInput

    if self.finish:
      if controller.health == 0:
        return "Death"

      if controller.isOnFloor:
        return "Idle"
      else:
        return "Jump"

    controller.velocity = controller.moveAndSlide(controller.velocity)

  method enter*(controlled: Node) =
    var controller: PlayerController = controlled as PlayerController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    var collider: Harmful = controller.hitbox.maxDamageCollider
    self.finish = false
    controller.health = max(0, controller.health - collider.damage.int)
    controller.playerBars.updateHealth(controller.health, controller.maxHealth)
    controller.velocity = Vector2(x: (controller.position.x - collider.globalPosition.x).signOrZero * collider.power.x, y: collider.power.y)
    discard controller.hitbox.timer.connect("timeout", self, "on_timer_timeout")
    controller.hitbox.timer.waitTime = collider.time
    controller.hitbox.timer.start
    stateMachine.travel("Hurt")
    self.main.animation.play("Pixelize")

  method exit*(controlled: Node) =
    var controller: PlayerController = controlled as PlayerController
    controller.hitbox.timer.disconnect("timeout", self, "on_timer_timeout")
    controller.velocity = Vector2(x: 0.0, y: 0.0)
    controller.hitbox.deactivate

  proc onTimerTimeout*(){.gdExport.} =
    self.finish = true
