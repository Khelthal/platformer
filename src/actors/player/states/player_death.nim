import
  godot,
  godotapi / [node],
  main,
  input / [i_input],
  states / [i_state],
  actors/player / [player_controller, player_input]

export
  i_state

gdobj PlayerDeath of IState:
  var
    finish: bool

  method update*(delta: float64, input: IInput, controlled: Node): string =
    var controller: PlayerController = controlled as PlayerController

    if self.finish:
      return "Death"

  method enter*(controlled: Node) =
    self.finish = false
    var controller: PlayerController = controlled as PlayerController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    stateMachine.travel("Death")

  method exit*(controlled: Node) =
    self.main.reload

  proc onDeathFinished*(){.gdExport.} =
    self.finish = true
