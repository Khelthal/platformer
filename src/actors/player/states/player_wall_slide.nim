import
  godot,
  godotapi / [node],
  input / [i_input],
  states / [i_state],
  actors/player / [player_controller, player_input],
  math

export
  i_state

gdobj PlayerWallSlide of IState:

  method update*(delta: float64, input: IInput, controlled: Node): string =
    var controller: PlayerController = controlled as PlayerController
    let input: PlayerInput = input as PlayerInput

    controller.velocity += Vector2(y: 800 * delta)
    controller.velocity.y = min(controller.velocity.y, 100)
    controller.velocity.x = input.movDir.float32 * controller.speed
    controller.velocity = controller.moveAndSlide(controller.velocity, Vector2(y: -1.0))
    controller.position = Vector2(x: round(controller.position.x), y: controller.position.y)

    if controller.hitbox.isColliding:
      return "Hurt"

    if controller.isOnFloor:
      if input.movDir != 0:
        return "Run"
      else:
        return "Stand"
    if input.jump:
      return "WallJump"
    if not controller.isOnWall or input.movDir == 0:
      return "Jump"

  method enter*(controlled: Node) =
    var controller: PlayerController = controlled as PlayerController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    stateMachine.travel("WallSlide")

  method exit*(controlled: Node) =
    var controller: PlayerController = controlled as PlayerController
