import
  godot,
  godotapi / [node, node_2d],
  input / [i_input],
  states / [i_state],
  actors/player / [player_controller, player_input],
  math

export
  i_state

gdobj PlayerRun of IState:

  method update*(delta: float64, input: IInput, controlled: Node): string =
    var controller: PlayerController = controlled as PlayerController
    let input: PlayerInput = input as PlayerInput
    if controller.hitbox.isColliding:
      return "Hurt"
    controller.velocity.x = input.movDir.float32 * controller.speed
    if input.dash:
      return "Dash"
    if controller.velocity.x == 0:
      return "Stand"
    if input.movDir != 0:
        controller.sprite.scale = Vector2(x: input.movDir.float32, y: controller.scale.y)
    controller.velocity = controller.moveAndSlideWithSnap(controller.velocity, Vector2(y: 7.0), Vector2(y: -1.0))
    controller.position = Vector2(x: controller.position.x, y: controller.position.y.round)
    if not controller.isOnFloor or input.jump:
      return "Jump"
    if input.attack:
      return "SwordAttack"

  method enter*(controlled: Node) =
    var controller: PlayerController = controlled as PlayerController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    stateMachine.travel("Run")

  method exit*(controlled: Node) =
    var controller: PlayerController = controlled as PlayerController
    controller.velocity.y = 0.0
