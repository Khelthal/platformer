import
  godot,
  godotapi / [node],
  input / [i_input],
  states / [i_state],
  actors/player / [player_controller, player_input]

export
  i_state

gdobj PlayerJump of IState:
  var
    fall: bool

  method update*(delta: float64, input: IInput, controlled: Node): string =
    let input: PlayerInput = input as PlayerInput
    var controller: PlayerController = controlled as PlayerController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    if controller.hitbox.isColliding:
      return "Hurt"

    if input.dash:
      return "Dash"

    if input.stopJump and controller.velocity.y < 0:
      controller.velocity.y = 0.0

    if controller.velocity.y >= 0 and not self.fall:
      stateMachine.travel("Falling")
      self.fall = true

    controller.velocity += Vector2(y: 800 * delta)
    controller.velocity.y = min(controller.velocity.y, 400)

    if input.movDir != 0:
        controller.sprite.scale = Vector2(x: input.movDir.float32, y: controller.scale.y)

    controller.velocity.x = input.movDir.float32 * controller.speed
    controller.velocity = controller.moveAndSlide(controller.velocity, Vector2(y: -1.0))

    if controller.isOnFloor:
      if controller.velocity.x == 0:
        return "Stand"
      else:
        return "Run"
    if controller.velocity.y >= 0 and controller.isOnWall and input.movDir != 0:
      return "WallSlide"

  method enter*(controlled: Node) =
    self.fall = false
    var controller: PlayerController = controlled as PlayerController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    if controller.isOnFloor:
      controller.velocity.y = -controller.jumpSpeed
    stateMachine.travel("Jump")

  method exit*(controlled: Node) =
    var controller: PlayerController = controlled as PlayerController
