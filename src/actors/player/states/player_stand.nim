import
  godot,
  godotapi / [node],
  input / [i_input],
  states / [i_state],
  actors/player / [player_controller, player_input]

export
  i_state

gdobj PlayerStand of IState:

  method update*(delta: float64, input: IInput, controlled: Node): string =
    var controller: PlayerController = controlled as PlayerController
    let input: PlayerInput = input as PlayerInput
    if controller.hitbox.isColliding:
      return "Hurt"
    if not controller.isOnFloor:
      return "Jump"
    if input.dash:
      return "Dash"
    if input.jump:
      return "Jump"
    if input.attack:
      return "SwordAttack"
    if input.movDir != 0:
      return "Run"

  method enter*(controlled: Node) =
    var controller: PlayerController = controlled as PlayerController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    controller.velocity = Vector2(x: 0.0, y: 0.0)
    stateMachine.travel("Stand")

  method exit*(controlled: Node) =
    discard
