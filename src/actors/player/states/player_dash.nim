import
  godot,
  godotapi / [node],
  input / [i_input],
  states / [i_state],
  actors/player / [player_controller, player_input]

export
  i_state

gdobj PlayerDash of IState:
  var
    count: int
    energyCost: int = 30
    enoughEnergy: bool

  method update*(delta: float64, input: IInput, controlled: Node): string =
    var controller: PlayerController = controlled as PlayerController
    let input: PlayerInput = input as PlayerInput
    if input.stopDash or not self.enoughEnergy:
      if controller.isOnFloor:
        return "Stand"
      else:
        return "Jump"
    if self.count <= 0:
      if controller.isOnFloor:
        return "Stand"
      else:
        return "Jump"
    if input.jump:
      return "Jump"
    controller.velocity.x = controller.sprite.scale.x * controller.speed * 1.5
    controller.velocity = controller.moveAndSlideWithSnap(controller.velocity, Vector2(y: 7.0), Vector2(y: -1.0))
    self.count.dec

  method enter*(controlled: Node) =
    var controller: PlayerController = controlled as PlayerController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    self.enoughEnergy = controller.energy >= self.energyCost
    if self.enoughEnergy:
      controller.energy -= self.energyCost
      controller.playerBars.updateEnergy(controller.energy, controller.maxEnergy)
      controller.velocity = Vector2(x: 0.0, y: 0.0)
      stateMachine.travel("Dash")
      self.count = 15

  method exit*(controlled: Node) =
    discard
