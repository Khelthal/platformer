import
  godot,
  godotapi / [node],
  input / [i_input],
  states / [i_state],
  actors/player / [player_controller, player_input]

export
  i_state

gdobj PlayerWallJump of IState:
  var
    count: int = 0

  method update*(delta: float64, input: IInput, controlled: Node): string =
    let input: PlayerInput = input as PlayerInput
    var controller: PlayerController = controlled as PlayerController

    if controller.hitbox.isColliding:
      return "Hurt"

    if input.stopJump or controller.velocity.y >= 0:
      controller.velocity.y = 0.0
      return "Jump"

    if self.count > 6:
      return "Jump"

    if input.movDir.float32 != controller.sprite.scale.x:
      return "Jump"

    controller.velocity += Vector2(y: 800 * delta)
    controller.velocity = controller.moveAndSlide(controller.velocity, Vector2(y: -1.0))

    if controller.isOnFloor:
      return "Run"
    if controller.velocity.y >= 0 and controller.isOnWall and input.movDir != 0:
      return "WallSlide"
    self.count.inc

  method enter*(controlled: Node) =
    self.count = 0
    var controller: PlayerController = controlled as PlayerController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    controller.velocity = Vector2(x: -controller.sprite.scale.x * 100.0, y: -controller.jumpSpeed)
    stateMachine.travel("Jump")

  method exit*(controlled: Node) =
    var controller: PlayerController = controlled as PlayerController
