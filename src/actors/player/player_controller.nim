import
  godot,
  godotapi / [kinematic_body_2d, animation_tree, sprite, animation_node_state_machine_playback, animation_player, timer],
  actors / [hit_box],
  actors/player / [player_bars]

export
  kinematic_body_2d,
  animation_tree,
  animation_node_state_machine_playback,
  sprite,
  hit_box,
  player_bars

gdobj PlayerController of KinematicBody2D:
  var
    velocity*: Vector2
    jumpSpeed*: float32 = 350
    speed*: float32 = 200
    animation*: AnimationTree
    animationPlayer: AnimationPlayer
    sprite*: Sprite
    hitbox*: HitBox
    maxHealth*: int = 100
    health*: int = 100
    maxEnergy*: int = 100
    energy*: int = 100
    playerBars*: PlayerBars
    energyTimer: Timer

  method ready*() =
    self.animation = self.getNode("AnimationTree") as AnimationTree
    self.animationPlayer = self.getNode("AnimationPlayer") as AnimationPlayer
    self.sprite = self.getNode("Sprite") as Sprite
    self.hitbox = self.getNode("HitBox") as HitBox
    self.playerBars = self.getNode("CanvasLayer/Bars") as PlayerBars
    self.energyTimer = self.getNode("Timer") as Timer
    self.position = Vector2(x: 50, y: 122)
    discard self.energyTimer.connect("timeout", self, "on_energy_timer_timeout")
    
    discard self.hitbox.connect("invulnerable", self, "on_hit_box_invulnerable")
    discard self.hitbox.connect("vulnerable", self, "on_hit_box_vulnerable")

  proc onHitBoxInvulnerable*(){.gdExport.} =
    self.animationPlayer.play("Invulnerable")

  proc onHitBoxVulnerable*(){.gdExport.} =
    self.animationPlayer.play("Vulnerable")

  proc onEnergyTimerTimeout*(){.gdExport.} =
    self.energy = min(self.maxEnergy, self.energy+10)
    self.playerBars.updateEnergy(self.energy, self.maxEnergy)
