import
  godot,
  godotapi / [kinematic_body_2d, animation_tree, sprite, animation_node_state_machine_playback, ray_cast_2d, timer],
  actors / [hit_box],
  actors/player / [player_controller]

export
  kinematic_body_2d,
  animation_tree,
  animation_node_state_machine_playback,
  sprite,
  hit_box,
  ray_cast_2d,
  timer

gdobj NightborneController of KinematicBody2D:
  var
    velocity*: Vector2
    speed*: float32 = 200
    animation*: AnimationTree
    sprite*: Sprite
    hitbox*: HitBox
    raycast*: RayCast2D
    target*: KinematicBody2D
    timer*: Timer
    initialPositionX*: float32

  method ready*() =
    self.animation = self.getNode("AnimationTree") as AnimationTree
    self.sprite = self.getNode("Sprite") as Sprite
    self.hitbox = self.getNode("HitBox") as HitBox
    self.raycast = self.sprite.getNode("RayCast2D") as RayCast2D
    self.timer = self.getNode("Timer") as Timer
    self.initialPositionX = self.position.x

  method process*(delta: float64) =
    if self.target.isNil:
      if self.raycast.isColliding:
        let collider = self.raycast.getCollider()
        if collider of PlayerController:
          self.target = collider as KinematicBody2D
    elif abs(self.position.x - self.target.position.x) > 200.0 or abs(self.position.y - self.target.position.y) > 80.0:
      self.target = nil
