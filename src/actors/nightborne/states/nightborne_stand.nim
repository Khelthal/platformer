import
  godot,
  godotapi / [node],
  input / [i_input],
  states / [i_state],
  actors/nightborne / [nightborne_controller, nightborne_input]

export
  i_state

gdobj NightborneStand of IState:

  method update*(delta: float64, input: IInput, controlled: Node): string =
    var input: NightborneInput = input as NightborneInput

    if input.attack:
      return "Attack"
    if input.movDir != 0:
      return "Run"

  method enter*(controlled: Node) =
    var controller: NightborneController = controlled as NightborneController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    stateMachine.travel("Stand")

  method exit*(controlled: Node) =
    discard
