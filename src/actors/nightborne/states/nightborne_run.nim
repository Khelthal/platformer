import
  godot,
  godotapi / [node],
  input / [i_input],
  states / [i_state],
  actors/nightborne / [nightborne_controller, nightborne_input]

export
  i_state

gdobj NightborneRun of IState:

  method update*(delta: float64, input: IInput, controlled: Node): string =
    var
      controller: NightborneController = controlled as NightborneController
      input: NightborneInput = input as NightborneInput

    if input.attack:
      return "Attack"
    if input.movDir == 0:
      return "Stand"

    controller.velocity = Vector2(x: input.movDir.float32 * controller.speed)
    if input.movDir != 0:
      controller.sprite.scale = Vector2(x: input.movDir.float32, y: controller.scale.y)
    controller.velocity = controller.moveAndSlideWithSnap(controller.velocity, Vector2(y: 7.0), Vector2(y: -1.0))

  method enter*(controlled: Node) =
    var controller: NightborneController = controlled as NightborneController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    stateMachine.travel("Run")

  method exit*(controlled: Node) =
    discard
