import
  godot,
  godotapi / [node],
  input / [i_input],
  states / [i_state],
  actors/nightborne / [nightborne_controller, nightborne_input]

export
  i_state

gdobj NightborneAttack of IState:
  var
    finish: bool

  method update*(delta: float64, input: IInput, controlled: Node): string =
    if self.finish:
      return "Stop"

  method enter*(controlled: Node) =
    self.finish = false
    var controller: NightborneController = controlled as NightborneController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    stateMachine.travel("Attack1")

  method exit*(controlled: Node) =
    discard

  proc onAttackFinished*(){.gdExport.} =
    self.finish = true
