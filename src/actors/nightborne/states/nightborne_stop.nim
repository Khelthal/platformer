import
  godot,
  godotapi / [node],
  input / [i_input],
  states / [i_state],
  actors/nightborne / [nightborne_controller, nightborne_input]

export
  i_state

gdobj NightborneStop of IState:
  var
    finish: bool

  method update*(delta: float64, input: IInput, controlled: Node): string =
    var input: NightborneInput = input as NightborneInput

    if self.finish:
      return "Stand"

  method enter*(controlled: Node) =
    self.finish = false
    var controller: NightborneController = controlled as NightborneController
    var stateMachine: AnimationNodeStateMachinePlayback = asNimGodotObject[AnimationNodeStateMachinePlayback](controller.animation.getImpl("parameters/playback").asGodotObject)
    stateMachine.travel("Stand")
    discard controller.timer.connect("timeout", self, "on_nightborne_timeout")
    controller.timer.start()

  method exit*(controlled: Node) =
    var controller: NightborneController = controlled as NightborneController
    controller.timer.disconnect("timeout", self, "on_nightborne_timeout")
    controller.timer.stop

  proc onNightborneTimeout(){.gdExport.} =
    self.finish = true
