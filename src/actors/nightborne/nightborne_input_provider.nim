import
  godot,
  godotapi / [input, node],
  input / [i_input_provider],
  actors/nightborne / [nightborne_input, nightborne_controller],
  actors/player / [player_controller],
  util / math

export
  nightborne_input

gdobj NightborneInputProvider of IInputProvider:
  
  method getInput*(controller: Node): IInput =
    var
      input: NightborneInput = gdnew[NightborneInput]()
      controller: NightborneController = controller as NightborneController
    input.movDir = 0
    input.attack = false
    if not controller.target.isNil:
      input.movDir = signOrZero(controller.target.position.x - controller.position.x).int
      input.attack = abs(controller.target.position.x - controller.position.x) < 30.0 and signOrZero(controller.sprite.scale.x) == signOrZero(controller.target.position.x - controller.position.x)
    return input
