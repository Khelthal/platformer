import
  godot,
  input / [i_input]

export
  i_input

gdobj NightborneInput of IInput:
  var
    attack*: bool
    movDir*: int
