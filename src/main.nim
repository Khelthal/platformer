import
  godot,
  godotapi / [node, scene_tree, animation_player],
  singleton / [map]

export
  map,
  animation_player

gdobj Main of Node:
  var
    map*: Map
    animation*: AnimationPlayer

  method init*() =
    self.map = gdnew[Map]()
    discard self.connect("tree_exiting", self, "on_main_tree_exiting")

  method ready*() =
    self.animation = self.getNode("AnimationPlayer") as AnimationPlayer

  proc onMainTreeExiting*(){.gdExport.} =
    self.map.queueFree

  proc reload*() =
    discard self.getTree.reloadCurrentScene

proc main*(self: Node): Main =
  return self.getTree.root.getChild(0) as Main
