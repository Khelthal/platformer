import
  godot,
  main,
  godotapi / [node_2d],
  util / [math],
  states / [i_state],
  input / [i_input],
  cams / [camera_controller],
  strutils

gdobj CameraVerticalExit of IState:
  var
    acc*: float32 = 0.0

  method update*(delta: float64, input: IInput, controlled: Node): string =
    let cam = controlled as CameraController
    let target = cam.followed.position
    
    let targetGrid = toGrid(target, cam.width, cam.height)
    cam.position = Vector2(x: targetGrid.x * cam.width.float32 + cam.width.float32 / 2.0, y: cam.position.y + aproximate(cam.position.y, targetGrid.y * cam.height.float32 + cam.height.float32 / 2.0, cam.spd + self.acc))
    self.acc += 0.1
    if cam.position.y == targetGrid.y * cam.height.float32 + cam.height.float32 / 2.0:
      return "Horizontal"
    return self.updateMode(cam)

  method enter*(controlled: Node) =
    self.acc = 0.0

  method exit*(controlled: Node) =
    discard

  proc updateMode(cam: CameraController): string =
    var
      pos: Vector2 = toGrid(cam.followed.position, cam.width, cam.height)
      coords: string = self.main.map.getCoords(pos)
    if coords == "":
      return
    let
      cameraMode: char = coords[0]
      x1: int = coords[1..2].parseInt
      x2: int = coords[3..4].parseInt
      y1: int = coords[5..6].parseInt
      y2: int = coords[7..8].parseInt
      v1: Vector2 = Vector2(x: x1.float32, y: y1.float32)
      v2: Vector2 = Vector2(x: x2.float32, y: y2.float32)

    pos = Vector2(x: pos.x * cam.width.float32, y: pos.y * cam.height.float32)
      
    if cam.followed.position.isBetween(pos + v1 * 16.0, pos + v2 * 16.0) and cameraMode == 'V':
      return "Vertical"

    if not cam.followed.position.isBetween(pos + v1 * 16.0, pos + v2 * 16.0) and cameraMode == 'H':
      return "Vertical"
