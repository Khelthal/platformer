import
  strutils,
  godot,
  godotapi / [camera_2d, scene_tree],
  util / [math]

gdobj CameraController of Camera2D:
  var
    width*{.gdExport.}: int
    height*{.gdExport.}: int
    spd*{.gdExport.}: float32 = 5.0
    followed*: Node2D
    ready*: bool

  method ready*() =
    var followed = self.getTree.getNodesInGroup("camera")[0]
    self.followed = asNimGodotObject[Node2D](followed.asGodotObject)
    self.position = toGrid(self.followed.position , self.width, self.height) + Vector2(x: self.width.float32/2.0, y: self.height.float32/2.0)
