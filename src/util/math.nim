import godot

proc toGrid*(vector: Vector2, width, height: int): Vector2 {.gcsafe, locks: 0.}
proc roundWithSign*(num: float32): float32 {.gcsafe, locks: 0.}
proc aproximate*(start, destiny, spd: float32): float32 {.gcsafe, locks: 0.}
proc isBetween*(self, lower, higher: Vector2): bool {.gcsafe, locks: 0.}
proc signOrZero*(num: float): float32 {.gcsafe, locks: 0.}
proc percentage*(total, fraction: int): float32 {.gcsafe, locks: 0.}

proc toGrid*(vector: Vector2, width, height: int): Vector2 =
  result.x = roundWithSign(vector.x / float32(width))
  result.y = roundWithSign(vector.y / float32(height))

proc roundWithSign*(num: float32): float32 =
  if num < 0:
    if float32(int(num)) == num:
      return num
    return float32(int(num) - 1)
  else:
    return float32(int(num))

proc aproximate*(start, destiny, spd: float32): float32 =
  result = destiny - start
  if abs(result) > spd:
    result = spd * sign(result)

proc isBetween*(self, lower, higher: Vector2): bool =
  if not (self.x > lower.x and self.x < higher.x):
    return false
  if not (self.y > lower.y and self.y < higher.y):
    return false
  return true

proc signOrZero*(num: float): float32 =
  if num > 0.0:
    return 1.0
  elif num < 0.0:
    return -1.0
  else:
    return 0.0

proc percentage*(total, fraction: int): float32 =
  return (fraction / total) * 100.0
