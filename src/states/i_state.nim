import
  godot,
  godotapi / [node],
  input / [i_input]

gdobj IState of Node:

  method update*(delta: float64, input: IInput, controlled: Node): string{.base.} =
    quit "to override!"

  method enter*(controlled: Node){.base.} =
    quit "to override!"

  method exit*(controlled: Node){.base.} =
    quit "to override!"
