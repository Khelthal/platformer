import
  godot,
  godotapi / [node],
  states / [i_state]

gdobj StateContainer of Node:
  var
    initialState*: IState
    debug*{.gdExport}: bool = false
    name*{.gdExport}: string

  method ready*() =
    self.initialState = self.getChild(0) as IState
