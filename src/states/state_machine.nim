import
  godot,
  godotapi / [node],
  states / [i_state, state_container],
  input / [i_input_provider, i_input]

gdobj StateMachine of Node:
  var
    state*: IState
    stateContainer*: StateContainer
    inputProvider*: IInputProvider
    controlled*: Node

  method ready*() =
    for i in 0..<self.getChildCount:
      let child: Node = self.getChild(i) as Node
      if child of IInputProvider:
        self.inputProvider = child as IInputProvider
      elif child of StateContainer:
        self.stateContainer = child as StateContainer
        self.state = self.stateContainer.initialState
      else:
        self.controlled = child

  method physicsProcess*(delta: float64) =
    let input: IInput = self.inputProvider.getInput(self.controlled)
    let newState: string = self.state.update(delta, input, self.controlled)
    self.setState(newState)
    input.queueFree

  method setState*(state: string){.base.} =
    if state != "":
      if self.stateContainer.debug:
        print(self.stateContainer.name, ": ", state)
      self.state.exit(self.controlled)
      self.state = self.stateContainer.getNode(state) as IState
      self.state.enter(self.controlled)
