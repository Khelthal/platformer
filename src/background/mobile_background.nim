import godot
import godotapi / [parallax_layer]

gdobj MobileBackground of ParallaxLayer:
  var
    velocity*{.gdExport.}: Vector2

  method process*(delta: float64) =
    var
      xOffset = self.motionOffset.x + self.velocity.x
      yOffset = self.motionOffset.y + self.velocity.y
    if xOffset >= self.motionMirroring.x:
      xOffset = xOffset - self.motionMirroring.x

    if yOffset >= self.motionMirroring.y:
      yOffset = yOffset - self.motionMirroring.y
    self.motionOffset = Vector2(x: xOffset, y: yOffset)
