import
  godot,
  godotapi / [node],
  input / [i_input]

export
  i_input,
  node

gdobj IInputProvider of Node:
  
  method getInput*(controller: Node): IInput {.base.} =
    quit "to override!"
