import godot
import godotapi / [node]
import input / [i_input]

gdobj IInputHandler of Node:

  method handleInput*(input: IInput): bool {.base.} =
    quit "to override!"
