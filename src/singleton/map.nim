import
  godot,
  godotapi / [node]

type
  MapLimits = array[4, array[4, string]]

gdobj Map of Node:
  var
    map*: MapLimits

  method init*() =
    self.map[0][0] = "L:H00280016"
    self.map[0][1] = "RU:V12280016"
    self.map[1][0] = "X"
    self.map[1][1] = "LR:H00280115"
    self.map[2][1] = "LR:H00280116"

  proc getRestrictions*(position: Vector2): string =
    return self.map[int(position.y)][int(position.x)]

  proc getLimits*(position: Vector2): string =
    let restrictions: string = self.map[int(position.y)][int(position.x)]
    if restrictions.find(':') == -1:
      return restrictions
    else:
      return restrictions[0..<(restrictions.find(':'))]

  proc getCoords*(position: Vector2): string =
    let restrictions: string = self.map[int(position.y)][int(position.x)]
    if restrictions.find(':') == -1:
      return ""
    else:
      return restrictions[(restrictions.find(':') + 1)..^1]

  proc getMode*(position: Vector2): char =
    let coords: string = self.getCoords(position)
    if coords == "":
      return ' '
    else:
      return coords[0]
