# Copyright 2017 Xored Software, Inc.

## Import independent scene objects here
## (no need to import everything, just independent roots)

when not defined(release):
  import segfaults # converts segfaults into NilAccessError

import
  states/state_machine,
  actors / [hit_box],
  actors/player / [player_controller, player_input_provider, player_bars],
  actors/player/states / [player_stand, player_run, player_jump, player_wall_slide, player_wall_jump, player_sword_attack, player_hurt, player_dash, player_death],
  actors/nightborne / [nightborne_controller, nightborne_input_provider],
  actors/nightborne/states / [nightborne_stand, nightborne_run, nightborne_attack, nightborne_stop],
  cams / [camera_controller],
  cams/states / [camera_horizontal, camera_vertical, camera_horizontal_exit, camera_vertical_exit],
  background / [mobile_background],
  weapons / [harmful]

{. warning[UnusedImport]:off .}
